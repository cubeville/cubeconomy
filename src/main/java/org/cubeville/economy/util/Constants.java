package org.cubeville.economy.util;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import org.cubeville.economy.CubeConomy;

public class Constants {
    // Code name
    public static final String Codename = "Cubeville";

    // Nodes
    private static String[] nodes = new String[] {
        "System.Logging.Enabled:false",
        "System.Interest.Enabled:false",
        "System.Interest.Announce.Enabled:false",

        "System.Default.Account.Holdings:30.0",
        "System.Default.Currency.Major:[ 'Dollar', 'Dollars' ]",
        "System.Default.Currency.Minor:[ 'Coin', 'Coins' ]",

        "System.Formatting.Minor:true",
        "System.Formatting.Seperate:false",

        "System.Fees.Death.Enabled:false",
        "System.Fees.Death.Percentage:true",
        "System.Fees.Death.Amount:0.0",

        "System.Interest.Online:true",
        "System.Interest.Interval.Seconds:60",
        "System.Interest.Amount.Cutoff:0.0",
        "System.Interest.Amount.Percent:0.0",
        "System.Interest.Amount.Minimum:1",
        "System.Interest.Amount.Maximum:2",

        "System.Database.Settings.Name:minecraft",
        "System.Database.Settings.Table:iConomy",

        "System.Database.Settings.MySQL.Hostname:localhost",
        "System.Database.Settings.MySQL.Port:3306",
        "System.Database.Settings.MySQL.Username:root",
        "System.Database.Settings.MySQL.Password:none",
    };

    // Files and Directories
    public static File Configuration;
    public static String Plugin_Directory;

    // iConomy basics
    public static List<String> Major = new LinkedList<String>();
    public static List<String> Minor = new LinkedList<String>();
    public static double Holdings = 30.0;

    // System formatting
    public static boolean FormatMinor = false;
    public static boolean FormatSeperated = false;

    // System Logging
    public static boolean Logging = false;

    // System Fees
    public static boolean DeathFee = false;
    public static boolean DeathFeePercentage = true;
    public static double DeathFeeAmount = 0.0;

    // System Interest
    public static int InterestSeconds = 60;
    public static boolean Interest = false;
    public static boolean InterestAnn = false;
    public static boolean InterestOnline = false;
    public static double InterestCutoff = 0.0;
    public static double InterestPercentage = 0.0;
    public static double InterestMin = 1;
    public static double InterestMax = 2;

    // Relational SQL Generics
    public static String SQLHostname = "localhost";
    public static String SQLPort = "3306";
    public static String SQLUsername = "root";
    public static String SQLPassword = "";

    // SQL Generics
    public static String SQLDatabase = "minecraft";
    public static String SQLTable = "economy";

    public static void load(FileConfiguration config) {
        Major.add("Dollar"); Major.add("Dollars");
        Minor.add("Coin"); Minor.add("Coins");

        // System Configuration
        Major = config.getStringList("System.Default.Currency.Major");
        Minor = config.getStringList("System.Default.Currency.Minor");
        Holdings = config.getDouble("System.Default.Account.Holdings", Holdings);

        // System Logging
        Logging = config.getBoolean("System.Logging.Enabled", Logging);

        // Formatting
        FormatMinor = config.getBoolean("System.Formatting.Minor", FormatMinor);
        FormatSeperated = config.getBoolean("System.Formatting.Seperate", FormatSeperated);

        // System Fees
        DeathFee = config.getBoolean("System.Fees.Death.Enabled", DeathFee);
        DeathFeePercentage = config.getBoolean("System.Fees.Death.Percentage", DeathFeePercentage);
        DeathFeeAmount = config.getDouble("System.Fees.Death.Amount", DeathFeeAmount);

        // System Interest
        Interest = config.getBoolean("System.Interest.Enabled", Interest);
        InterestOnline = config.getBoolean("System.Interest.Online", InterestOnline);
        InterestAnn = config.getBoolean("System.Interest.Announce.Enabled", InterestAnn);
        InterestSeconds = config.getInt("System.Interest.Interval.Seconds", InterestSeconds);
        InterestPercentage = config.getDouble("System.Interest.Amount.Percent", InterestPercentage);
        InterestCutoff = config.getDouble("System.Interest.Amount.Cutoff", InterestCutoff);
        InterestMin = config.getDouble("System.Interest.Amount.Minimum", InterestMin);
        InterestMax = config.getDouble("System.Interest.Amount.Maximum", InterestMax);

        // Database
        SQLDatabase = config.getString("System.Database.Settings.Name", SQLDatabase);
        SQLTable = config.getString("System.Database.Settings.Table", SQLTable);

        // MySQL
        SQLHostname = config.getString("System.Database.Settings.MySQL.Hostname", SQLHostname);
        SQLPort = config.getString("System.Database.Settings.MySQL.Port", SQLPort);
        SQLUsername = config.getString("System.Database.Settings.MySQL.Username", SQLUsername);
        SQLPassword = config.getString("System.Database.Settings.MySQL.Password", SQLPassword);

        int i = 0;

        for(String node : nodes) {
            if(config.get(node.split(":")[0]) == null) {
                i++;
            }
        }

        if(i != 0) {
            CubeConomy.logger().info("Configuration Integrity Start:");

            for(String node : nodes) {
                if(config.get(node.split(":")[0]) == null) {
                    System.out.println("    - "+ node.split(":")[0] +" is null or missing, Defaulting to: " + node.split(":")[1]);
                }
            }

            CubeConomy.logger().info("Configuration Integrity End.");
        }
    }
}