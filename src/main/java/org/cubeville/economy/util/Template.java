package org.cubeville.economy.util;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import org.cubeville.economy.CubeConomy;

public class Template {

    private File file = null;
    private FileConfiguration tpl = null;

    public Template(String directory, String filename) {
        file = new File(directory, filename);
        tpl = YamlConfiguration.loadConfiguration(new File(directory, filename));

        upgrade();
    }

    public void upgrade() {
        LinkedHashMap<String, String> nodes = new LinkedHashMap<String, String>();

        if(tpl.getString("accounts.create") == null) {
            nodes.put("accounts.create","<green>Created account with the name: <white>+name<green>.");
            nodes.put("accounts.remove","<green>Deleted account: <white>+name<green>.");
            nodes.put("error.exists","<rose>Account already exists.");
        }

        if(tpl.getString("accounts.status") == null) {
            nodes.put("error.online","<rose>Sorry, nobody else is online.");
            nodes.put("accounts.status", "<green>Account status is now: <white>+status<green>.");
        }

        if(tpl.getString("interest.announcement") == null) {
            nodes.put("interest.announcement", "+amount <green>interest gained.");
        }

        if (tpl.getString("player.death") == null) {
            nodes.put("player.death", "<rose>You lost <white>+amount<rose> for dying.");
        }

        if(!nodes.isEmpty()) {
            System.out.println(" - Upgrading Template.yml");
            int count = 1;

            for(String node : nodes.keySet()) {
                System.out.println("   Adding node [" + node + "] #" + count + " of " + nodes.size());
                tpl.set(node, nodes.get(node));
                count++;
            }

            try {
                tpl.save(file);
            } catch (IOException ex) {
                CubeConomy.logger().warning("Could not save template.yml");
                return;
            }

            CubeConomy.logger().info(" + Messages Upgrade Complete.");
        }
    }

    /**
     * Grab the raw template line by the key, and don't save anything.
     *
     * @param key The template key we wish to grab.
     *
     * @return <code>String</code> - Template line / string.
     */
    public String raw(String key) {
        return tpl.getString(key);
    }

    /**
     * Grab the raw template line and save data if no key existed.
     *
     * @param key The template key we are searching for.
     * @param line The line to be placed if no key was found.
     *
     * @return
     */
    public String raw(String key, String line) {
        return tpl.getString(key, line);
    }

    public void save(String key, String line) throws IOException {
        tpl.set(key, line);
        tpl.save(file);
    }

    public String color(String key) {
        return Messaging.parse(Messaging.colorize(this.raw(key)));
    }

    public String parse(String key, Object[] argument, Object[] points) {
        return Messaging.parse(Messaging.colorize(Messaging.argument(this.raw(key), argument, points)));
    }

    public String parse(String key, String line, Object[] argument, Object[] points) {
        return Messaging.parse(Messaging.colorize(Messaging.argument(this.raw(key, line), argument, points)));
    }
}
