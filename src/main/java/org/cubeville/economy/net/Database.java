package org.cubeville.economy.net;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.util.Constants;

public class Database {
    private String driver;
    private String dsn;
    private String username;
    private String password;

    public Database() {
        driver = "com.mysql.jdbc.Driver";
        dsn = "jdbc:mysql://" + Constants.SQLHostname + ":" + Constants.SQLPort + "/" + Constants.SQLDatabase;
        username = Constants.SQLUsername;
        password = Constants.SQLPassword;

        try {
            Class.forName(driver).newInstance();
        } catch (Exception e) { CubeConomy.logger().severe("Driver error: " + e); }
    }

    public Connection getConnection() {
        try {
            if(username.equalsIgnoreCase("") && password.equalsIgnoreCase(""))
                return (DriverManager.getConnection(dsn));
            else
                return (DriverManager.getConnection(dsn, username, password));
        } catch (SQLException e) {
            CubeConomy.logger().severe("Could not create connection: " + e);
            return null;
        }
    }

    /**
     * Create the accounts table if it doesn't exist already.
     * @throws Exception
     */
    public void setupAccountTable() throws Exception {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

            DatabaseMetaData dbm = conn.getMetaData();
            rs = dbm.getTables(null, null, Constants.SQLTable, null);

        if (!rs.next()) {
            CubeConomy.logger().info("Creating table: " + Constants.SQLTable);

            ps = conn.prepareStatement(
                "CREATE TABLE " + Constants.SQLTable + " (" +
                    "`id` INT(10) NOT NULL AUTO_INCREMENT," +
                    "`username` VARCHAR(32) NOT NULL," +
                    "`balance` DECIMAL(64, 2) NOT NULL," +
                    "`hidden` BOOLEAN NOT NULL DEFAULT '0'," +
                    "PRIMARY KEY (`id`)," +
                    "UNIQUE(`username`)" +
                ")"
            );

            if(ps != null) {
                ps.executeUpdate();
            }

            CubeConomy.logger().info("Table Created.");
        }

        close(ps, rs, conn);
    }

    public void setupTransactionTable() throws Exception {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (Constants.Logging) {
            DatabaseMetaData dbm = conn.getMetaData();
            rs = dbm.getTables(null, null, Constants.SQLTable + "_Transactions", null);

            if (!rs.next()) {
                CubeConomy.logger().info("Creating logging database.. [" + Constants.SQLTable + "_Transactions]");

                ps = conn.prepareStatement(
                    "CREATE TABLE " + Constants.SQLTable + "_Transactions ("
                        + "`id` INT(255) NOT NULL AUTO_INCREMENT, "
                        + "`account_from` TEXT NOT NULL, "
                        + "`account_to` TEXT NOT NULL, "
                        + "`amount` DECIMAL(65, 2) NOT NULL, "
                        + "`timestamp` TEXT NOT NULL, "
                        + "PRIMARY KEY (`id`)" +
                    ");"
                );

                if(ps != null) {
                    ps.executeUpdate();
                    CubeConomy.logger().info("Database Created.");
                }

                CubeConomy.logger().info("Logging enabled.");
            }
        } else {
            CubeConomy.logger().info("Logging is currently disabled.");
        }

        close(ps, rs, conn);
    }

    /**
     * Attempts to close the specified prepared statement
     *
     * @param ps Statement to close
     */
    public void close(Statement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) { }
        }
    }

    /**
     * Attempts to close the specified result set
     *
     * @param rs ResultSet to close
     */
    public void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) { }
        }
    }

    /**
     * Attempts to close the specified connection
     *
     * @param connection Connection to close
     */
    public void close(Connection connection) {
        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) { }
        }
    }

    /**
     * Attempts to close the specified database resources
     *
     * @param ps Statement to close
     * @param conn Connection to close
     */
    public void close(Statement ps, Connection conn) {
        close(ps);
        close(conn);
    }

    /**
     * Attempts to close the specified database resources
     *
     * @param ps Statement to close
     * @param rs ResultSet to close
     * @param conn Connection to close
     */
    public void close(Statement ps, ResultSet rs, Connection conn) {
        close(ps);
        close(rs);
        close(conn);
    }

}
