package org.cubeville.economy.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class AccountRemoveEvent extends AccountEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private boolean cancelled = false;

    public AccountRemoveEvent(String account) {
        super(account);
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
