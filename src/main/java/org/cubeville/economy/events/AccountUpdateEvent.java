package org.cubeville.economy.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class AccountUpdateEvent extends AccountEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private double balance;
    private double previous;
    private double amount;
    private boolean cancelled = false;

    public AccountUpdateEvent(String account, double previous, double balance, double amount) {
        super(account);
        this.previous = previous;
        this.balance = balance;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
        balance = previous+amount;
    }

    public double getPrevious() {
        return previous;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
