package org.cubeville.economy.events;

import org.bukkit.event.Event;

public abstract class AccountEvent extends Event {

    private final String account;

    public AccountEvent(String account) {
        this.account = account;
    }

    /**
     * Gets the name of the account related to this event
     *
     * @return Account name
     */
    public final String getAccountName() {
        return account;
    }

}
