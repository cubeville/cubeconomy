package org.cubeville.economy.events;

import org.bukkit.event.HandlerList;

public class AccountSetEvent extends AccountEvent {
    private static final HandlerList handlers = new HandlerList();

    private double balance;

    public AccountSetEvent(String account, double balance) {
        super(account);
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
