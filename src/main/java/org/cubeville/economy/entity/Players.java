package org.cubeville.economy.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.plugin.PluginManager;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.events.AccountResetEvent;
import org.cubeville.economy.events.AccountSetEvent;
import org.cubeville.economy.events.AccountUpdateEvent;
import org.cubeville.economy.system.Account;
import org.cubeville.economy.system.Holdings;
import org.cubeville.economy.util.Constants;
import org.cubeville.economy.util.Messaging;
import org.cubeville.economy.util.Misc;
import org.cubeville.economy.util.Template;

/**
 * Handles the command usage and account creation upon a player joining the
 * server. Also handles player death fees if enabled and sending balance
 * updates to the client whenever their balance changes.
 *
 * @author Nijikokun
 * @author GoalieGuy6
 */
public class Players implements Listener {
    private boolean registered = false;
    private Template Template = null;
    private DeathListener deathListener = null;

    /**
     * Initialize the class as well as the template for various
     * messages throughout the commands.
     *
     * @param directory
     */
    public Players(String directory) {
        Template = new Template(directory, "template.yml");
    }

    public void registerEvents(PluginManager pm, CubeConomy plugin) {
        if (!registered) {
            pm.registerEvents(this, plugin);
            registered = true;
        }

        if (Constants.DeathFee && deathListener == null) {
            deathListener = new DeathListener();
            pm.registerEvents(deathListener, plugin);
        }
    }

    /**
     * Help documentation for iConomy all in one method.
     *
     * Allows us to easily utilize all throughout the class without having multiple
     * instances of the same help lines.
     */
    private void getMoneyHelp(CommandSender player) {
        Messaging.send("&e ");
        Messaging.send("&f CubeConomy (&c" + Constants.Codename + "&f)");
        Messaging.send("&e ");
        Messaging.send("&f [] Required, () Optional");
        Messaging.send(" ");
        Messaging.send("`G  /money &e Check your balance");
        Messaging.send("`G  /money `g? &e For help & Information");

        if (CubeConomy.hasPermissions(player, "iConomy.rank")) {
            Messaging.send("`G  /money `grank `G(`wplayer`G) &e Rank on the topcharts.   ");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.list")) {
            Messaging.send("`G  /money `gtop `G(`wamount`G) &e Richest players listing.  ");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.payment")) {
            Messaging.send("`G  /money `gpay `G[`wplayer`G] [`wamount`G] &e Send money to a player.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.grant")) {
            Messaging.send("`G  /money `ggrant `G[`wplayer`G] [`wamount`G] &e Give money.");
            Messaging.send("`G  /money `ggrant `G[`wplayer`G] -[`wamount`G] &e Take money.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.set")) {
            Messaging.send("`G  /money `gset `G[`wplayer`G] [`wamount`G] &e Sets a players balance.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.hide")) {
            Messaging.send("`G  /money `ghide `G[`wplayer`G] `wtrue`G/`wfalse &e Hide or show an account.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.account.create")) {
            Messaging.send("`G  /money `gcreate `G[`wplayer`G] &e Create player account.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.account.remove")) {
            Messaging.send("`G  /money `gremove `G[`wplayer`G] &e Remove player account.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.reset")) {
            Messaging.send("`G  /money `greset `G[`wplayer`G] &e Reset player account.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.purge")) {
            Messaging.send("`G  /money `gpurge &e Remove all accounts with inital holdings.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.empty")) {
            Messaging.send("`G  /money `gempty &e Empties database.");
        }

        if (CubeConomy.hasPermissions(player, "iConomy.admin.stats")) {
            Messaging.send("`G  /money `gstats &e Check all economic stats.");
        }

        Messaging.send(" ");
    }

    public boolean setHidden(String name, boolean hidden) {
        return CubeConomy.getAccount(name).setHidden(hidden);
    }

    /**
     * Account Creation
     */
    public void createAccount(String name) {
        CubeConomy.getAccount(name);
        Messaging.send(Template.color("tag.money") + Template.parse("accounts.create", new String[]{ "+name,+n" }, new String[]{ name }));
    }

    /**
     * Account Removal
     */
    public void removeAccount(String name) {
        CubeConomy.Accounts.remove(name);
        Messaging.send(Template.color("tag.money") + Template.parse("accounts.remove", new String[]{ "+name,+n" }, new String[]{ name }));
    }

    /**
     * Shows the balance to the requesting player.
     *
     * @param name The name of the player we are viewing
     * @param viewing The player who is viewing the account
     * @param mine Is it the player who is trying to view?
     */
    public void showBalance(String name, CommandSender viewing, boolean mine) {
        if (mine) {
            Messaging.send(viewing, Template.color("tag.money") + Template.parse("personal.balance", new String[]{"+balance,+b"}, new String[]{ CubeConomy.format(name) }));
        } else {
            Messaging.send(viewing, Template.color("tag.money") + Template.parse("player.balance", new String[]{"+balance,+b", "+name,+n"}, new String[]{ CubeConomy.format(name), name }));
        }
    }

    /**
     * Reset a players account easily.
     *
     * @param resetting The player being reset. Cannot be null.
     * @param by The player resetting the account. Cannot be null.
     * @param notify Do we want to show the updates to each player?
     */
    public void showPayment(String from, String to, double amount) {
        Player paymentFrom = Bukkit.getPlayer(from);
        Player paymentTo = Bukkit.getPlayer(to);

        if(paymentFrom != null) {
            from = paymentFrom.getName();
        }

        if(paymentTo != null) {
            to = paymentTo.getName();
        }

        Holdings From = CubeConomy.getAccount(from).getHoldings();
        Holdings To = CubeConomy.getAccount(to).getHoldings();

        if (from.equals(to)) {
            if (paymentFrom != null) {
                Messaging.send(paymentFrom, Template.color("payment.self"));
            }
        } else if (amount < 0.0 || !From.hasEnough(amount)) {
            if (paymentFrom != null) {
                Messaging.send(paymentFrom, Template.color("error.funds"));
            }
        } else {
            From.subtract(amount);
            To.add(amount);

            CubeConomy.getTransactions().insert(from, to, amount);

            if (paymentFrom != null) {
                Messaging.send(
                        paymentFrom,
                        Template.color("tag.money") + Template.parse(
                        "payment.to",
                        new String[]{"+name,+n", "+amount,+a"},
                        new String[]{to, CubeConomy.format(amount)}));

                showBalance(from, paymentFrom, true);
            }

            if (paymentTo != null) {
                Messaging.send(
                        paymentTo,
                        Template.color("tag.money") + Template.parse(
                        "payment.from",
                        new String[]{"+name,+n", "+amount,+a"},
                        new String[]{from, CubeConomy.format(amount)}));

                showBalance(to, paymentTo, true);
            }
        }
    }

    /**
     * Reset a players account, accessable via Console & In-Game
     *
     * @param account The account we are resetting.
     * @param controller If set to null, won't display messages.
     * @param console Is it sent via console?
     */
    public void showReset(String account, Player controller, boolean console) {
        Player player = Bukkit.getPlayer(account);

        if(player != null) {
            account = player.getName();
        }

        // Get account
        Account Account = CubeConomy.getAccount(account);

        // Log Transaction
        CubeConomy.getTransactions().insert(account, "[System]", Account.getHoldings().balance());

        // Reset
        Account.getHoldings().reset();

        if (player != null) {
            Messaging.send(player, Template.color("personal.reset"));
        }

        if (controller != null) {
            Messaging.send(
                Template.parse(
                    "player.reset",
                    new String[]{ "+name,+n" },
                    new String[]{ account }
                )
            );
        }

        if (console) {
            CubeConomy.logger().info("Player " + account + "'s account has been reset.");
        } else {
            CubeConomy.logger().info("Player " + account + "'s account has been reset by " + controller.getName() + ".");
        }
    }

    /**
     * Grants money to a player
     *
     * @param account The player to grant money to
     * @param controller The player granting the money
     * @param amount The amount of money being granted
     * @param console If the command was sent from the console
     */
    public void showGrant(String name, Player controller, double amount, boolean console) {
        Player online = Bukkit.getPlayer(name);

        if(online != null) {
            name = online.getName();
        }

        Account account = CubeConomy.getAccount(name);

        if(account != null) {
            Holdings holdings = account.getHoldings();
            holdings.add(amount);

            if (amount < 0.0) {
                CubeConomy.getTransactions().insert(name, "[System]", amount);
            } else {
                CubeConomy.getTransactions().insert("[System]", name, amount);
            }

            if (online != null) {
                Messaging.send(online,
                    Template.color("tag.money") + Template.parse(
                        (amount < 0.0) ? "personal.debit" : "personal.credit",
                        new String[]{"+by", "+amount,+a"},
                        new String[]{(console) ? "console" : controller.getName(), CubeConomy.format(((amount < 0.0) ? amount * -1 : amount))}
                    )
                );

                showBalance(name, online, true);
            }

            if (controller != null) {
                Messaging.send(
                    Template.color("tag.money") + Template.parse(
                        (amount < 0.0) ? "player.debit" : "player.credit",
                        new String[]{"+name,+n", "+amount,+a"},
                        new String[]{ name, CubeConomy.format(((amount < 0.0) ? amount * -1 : amount)) }
                    )
                );
            }

            if (console) {
                CubeConomy.logger().info("Player " + account.getName() + "'s account had " + ((amount < 0.0) ? "negative " : "") + CubeConomy.format(((amount < 0.0) ? amount * -1 : amount)) + " grant to it.");
            } else {
                CubeConomy.logger().info("Player " + account.getName() + "'s account had " + ((amount < 0.0) ? "negative " : "") + CubeConomy.format(((amount < 0.0) ? amount * -1 : amount)) + " grant to it by " + controller.getName() + ".");
            }
        }
    }

    /**
     * Sets the balance of an account
     *
     * @param account The account to set the balance of
     * @param controller The player setting the balance
     * @param amount The amount to set the balance to
     * @param console If the command was sent from the console
     */
    public void showSet(String name, Player controller, double amount, boolean console) {
        Player online = Bukkit.getPlayer(name);

        if(online != null) {
            name = online.getName();
        }

        Account account = CubeConomy.getAccount(name);

        if(account != null) {
            Holdings holdings = account.getHoldings();
            Double balance = holdings.balance();
            holdings.set(amount);

            Double diff = holdings.balance() - balance;

            if (diff < 0.0) {
                CubeConomy.getTransactions().insert(name, "[System]", diff);
            } else {
                CubeConomy.getTransactions().insert("[System]", name, diff);
            }

            if (online != null) {
                Messaging.send(online,
                    Template.color("tag.money") + Template.parse(
                        "personal.set",
                        new String[]{"+by", "+amount,+a"},
                        new String[]{(console) ? "Console" : controller.getName(), CubeConomy.format(amount) }
                    )
                );

                showBalance(name, online, true);
            }

            if (controller != null) {
                Messaging.send(
                    Template.color("tag.money") + Template.parse(
                        "player.set",
                        new String[]{ "+name,+n", "+amount,+a" },
                        new String[]{ name, CubeConomy.format(amount) }
                    )
                );
            }

            if (console) {
                CubeConomy.logger().info("Player " + account + "'s account had " + CubeConomy.format(amount) + " set to it.");
            } else {
                CubeConomy.logger().info("Player " + account + "'s account had " + CubeConomy.format(amount) + " set to it by " + controller.getName() + ".");
            }
        }
    }

    /**
     * Parses and outputs personal rank.
     *
     * Grabs rankings via the bank system and outputs the data,
     * using the template variables, to the given player stated
     * in the method.
     *
     * @param viewing The sender asking for the rank
     * @param player The account whose rank to get
     */
    public void showRank(CommandSender viewing, String player) {
        Account account = CubeConomy.getAccount(player);

        if (account != null) {
            int rank = account.getRank();
            boolean isPlayer = (viewing instanceof Player);
            boolean isSelf = (isPlayer) ? ((((Player)viewing).getName().equalsIgnoreCase(player)) ? true : false) : false;

            Messaging.send(
                viewing,
                Template.color("tag.money") + Template.parse(
                    ((isSelf) ? "personal.rank" : "player.rank"),
                    new Object[]{ "+name,+n", "+rank,+r" },
                    new Object[]{ player, rank }
                )
            );
        } else {
            Messaging.send(
                viewing,
                Template.parse(
                    "error.account",
                    new Object[]{ "+name,+n" },
                    new Object[]{ player }
                )
            );
        }
    }

    /**
     * Top ranking users by cash flow.
     *
     * Grabs the top amount of players and outputs the data, using the template
     * system, to the given viewing player.
     *
     * @param viewing The sender asking for the list
     * @param amount The number of top users to get
     */
    public void showTop(CommandSender viewing, int amount) {
        LinkedHashMap<String, Double> Ranking = CubeConomy.Accounts.ranking(amount);
        int count = 1;

        Messaging.send(
                viewing,
                Template.parse(
                "top.opening",
                new Object[]{ "+amount,+a" },
                new Object[]{ amount }
            )
        );

        if(Ranking == null || Ranking.isEmpty()) {
            Messaging.send(viewing, Template.color("top.empty"));

            return;
        }

        for (String account : Ranking.keySet()) {
            Double balance = Ranking.get(account);

            Messaging.send(
                viewing,
                Template.parse(
                    "top.line",
                    new String[]{"+i,+number", "+player,+name,+n", "+balance,+b"},
                    new Object[]{count, account, CubeConomy.format(balance)}
                )
            );

            count++;
        }
    }

    /**
     * Commands sent from in-game are parsed and evaluated here.
     *
     * @param sender The CommandSender that sent the command
     * @param split The input line split by spaces.
     */
    public void onPlayerCommand(CommandSender sender, String[] split) {
        Messaging.save(sender);
        boolean isPlayer = (sender instanceof Player);
        Player player = (sender instanceof Player) ? (Player)sender : null;

        if (split[0].equalsIgnoreCase("money")) {
            switch (split.length) {
                case 1:
                    if(isPlayer)
                        showBalance(player.getName(), player, true);
                    else {
                        Messaging.send("`RCannot show balance without organism.");
                    }

                    return;

                case 2:

                    if (Misc.is(split[1], new String[]{ "rank", "-r" })) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.rank") || !isPlayer) {
                            return;
                        }

                        showRank(player, player.getName());
                        return;
                    }

                    if (Misc.is(split[1], new String[]{ "top", "-t" })) {
                        if (!CubeConomy.hasPermissions(player, "iConomy.list")) {
                            return;
                        }

                        showTop(sender, 5);
                        return;
                    }

                    if (Misc.is(split[1], new String[]{ "empty", "-e" })) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.empty")) {
                            return;
                        }

                        CubeConomy.Accounts.emptyDatabase();

                        Messaging.send(Template.color("accounts.empty"));
                        return;
                    }

                    if (Misc.is(split[1], new String[]{ "purge", "-pf" })) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.purge")) {
                            return;
                        }

                        CubeConomy.Accounts.purge();
                        Messaging.send(Template.color("accounts.purge"));
                        return;
                    }

                    if (Misc.is(split[1], new String[]{ "stats", "-s" })) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.stats")) {
                            return;
                        }

                        Collection<Double> accountHoldings = CubeConomy.Accounts.values();
                        Collection<Double> totalHoldings = accountHoldings;

                        double TCOH = 0;
                        int accounts = accountHoldings.size();
                        int totalAccounts = accounts;

                        for (Object o : totalHoldings.toArray()) {
                            TCOH += (Double)o;
                        }

                        Messaging.send(Template.color("statistics.opening"));

                        Messaging.send(Template.parse("statistics.total",
                                new String[]{ "+currency,+c", "+amount,+money,+a,+m" },
                                new Object[]{ Constants.Major.get(1), CubeConomy.format(TCOH) }
                        ));

                        Messaging.send(Template.parse("statistics.average",
                                new String[]{ "+currency,+c", "+amount,+money,+a,+m" },
                                new Object[]{ Constants.Major.get(1), CubeConomy.format(TCOH / totalAccounts) }
                        ));

                        Messaging.send(Template.parse("statistics.accounts",
                                new String[]{ "+currency,+c", "+amount,+accounts,+a" },
                                new Object[]{ Constants.Major.get(1), accounts }
                        ));

                        return;
                    }

                    if (Misc.is(split[1], new String[]{
                        "help", "?", "grant", "-g", "reset", "-x",
                        "set", "-s", "pay", "-p", "create", "-c",
                        "remove", "-v", "hide", "-h" })) {
                        getMoneyHelp(player); return;
                    } else {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.access")) {
                            return;
                        }

                        Player online = Bukkit.getPlayer(split[1]);
                        if (online != null) {
                            if (sender instanceof Player && !((Player) sender).canSee(online)) {
                                online = null;
                            } else {
                                split[1] = online.getName();
                            }
                        }

                        if (CubeConomy.hasAccount(split[1])) {
                            showBalance(split[1], sender, false);
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[1]}));
                        }

                        return;
                    }

                case 3:

                    if (Misc.is(split[1], new String[]{"rank", "-r"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.rank")) {
                            return;
                        }

                        if (CubeConomy.hasAccount(split[2])) {
                            showRank(sender, split[2]);
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[2]}));
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"top", "-t"})) {
                        if (!CubeConomy.hasPermissions(player, "iConomy.list")) {
                            return;
                        }

                        try {
                            int top = Integer.parseInt(split[2]);
                            showTop(sender, top < 0 ? 5 : ((top > 100) ? 100 : top));
                        } catch (Exception e) {
                            showTop(sender, 5);
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"create", "-c"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.account.create")) {
                            return;
                        }

                        if (!CubeConomy.hasAccount(split[2])) {
                            createAccount(split[2]);
                        } else {
                            Messaging.send(Template.parse("error.exists", new String[]{"+name,+n"}, new String[]{split[2]}));
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"remove", "-v"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.account.remove")) {
                            return;
                        }

                        if (CubeConomy.hasAccount(split[2])) {
                            removeAccount(split[2]);
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[2]}));
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"reset", "-x"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.reset")) {
                            return;
                        }

                        if (CubeConomy.hasAccount(split[2])) {
                            if(isPlayer)
                                showReset(split[2], player, false);
                            else {
                                showReset(split[2], null, true);
                            }
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[2]}));
                        }

                        return;
                    }

                    break;

                case 4:

                    if (Misc.is(split[1], new String[]{"pay", "-p"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.payment") || !isPlayer) {
                            return;
                        }

                        String name = "";
                        double amount = 0.0;

                        if (CubeConomy.hasAccount(split[2])) {
                            name = split[2];
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[2]})); return;
                        }

                        try {
                            amount = Double.parseDouble(split[3]);

                            if (amount < 0.01) {
                                throw new NumberFormatException();
                            }
                        } catch (NumberFormatException ex) {
                            Messaging.send("&cInvalid amount: &f" + amount);
                            Messaging.send("&cUsage: &f/money &c[&f-p&c|&fpay&c] <&fplayer&c> &c<&famount&c>"); return;
                        }

                        showPayment(player.getName(), name, amount);
                        return;
                    }

                    if (Misc.is(split[1], new String[]{"grant", "-g"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.grant")) {
                            return;
                        }

                        ArrayList<String> accounts = new ArrayList<String>();
                        boolean console = (isPlayer) ? false : true;
                        double amount = 0.0;

                        Player check = Misc.playerMatch(split[2]);
                        String name = "";

                        if(check != null) {
                            name = check.getName();
                        } else {
                            name = split[2];
                        }

                        if (CubeConomy.hasAccount(name)) {
                            accounts.add(name);
                        } else {
                            Messaging.send(Template.parse("error.account", new String[]{ "+name,+n" }, new String[]{ name })); return;
                        }

                        try {
                            amount = Double.parseDouble(split[3]);
                        } catch (NumberFormatException e) {
                            Messaging.send("&cInvalid amount: &f" + split[3]);
                            Messaging.send("&cUsage: &f/money &c[&f-g&c|&fgrant&c] <&fplayer&c> (&f-&c)&c<&famount&c>"); return;
                        }

                        if(accounts.size() < 1 || accounts.isEmpty()) {
                            Messaging.send(Template.color("<rose>Grant Query returned 0 accounts to alter.")); return;
                        }

                        for(String account : accounts) {
                            showGrant(account, player, amount, console);
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"hide", "-h"})) {
                        if (!CubeConomy.hasPermissions(sender, "iConomy.admin.hide")) {
                            return;
                        }

                        String name = "";
                        Player check = Misc.playerMatch(split[2]);
                        boolean hidden = false;

                        if(check != null) {
                            name = check.getName();
                        } else {
                            name = split[2];
                        }

                        if (!CubeConomy.hasAccount(name)) {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{ split[2] })); return;
                        }

                        if (Misc.is(split[3], new String[]{"true", "t", "-t", "yes", "da", "-d"})) {
                            hidden = true;
                        }

                        if(!setHidden(name, hidden)) {
                            Messaging.send(Template.parse("error.account", new String[]{ "+name,+n" }, new String[]{ name }));
                        } else {
                            Messaging.send(Template.parse("accounts.status", new String[]{ "+status,+s" }, new String[]{ (hidden) ? "hidden" : "visible" }));
                        }

                        return;
                    }

                    if (Misc.is(split[1], new String[]{"set", "-s"})) {
                        if (!CubeConomy.hasPermissions(player, "iConomy.admin.set")) {
                            return;
                        }

                        String name = "";
                        double amount = 0.0;

                        Player check = Misc.playerMatch(split[2]);

                        if(check != null) {
                            name = check.getName();
                        } else {
                            name = split[2];
                        }

                        if (!CubeConomy.hasAccount(name)) {
                            Messaging.send(Template.parse("error.account", new String[]{"+name,+n"}, new String[]{split[2]})); return;
                        }

                        try {
                            amount = Double.parseDouble(split[3]);
                        } catch (NumberFormatException e) {
                            Messaging.send("&cInvalid amount: &f" + split[3]);
                            Messaging.send("&cUsage: &f/money &c[&f-g&c|&fgrant&c] <&fplayer&c> (&f-&c)&c<&famount&c>"); return;
                        }

                        if(isPlayer)
                            showSet(name, player, amount, false);
                        else {
                            showSet(name, null, amount, true);
                        }

                        return;
                    }

                    break;
            }

            getMoneyHelp(player);
        }

        return;
    }

    /**
     * Listen for new players and create their accounts
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if (CubeConomy.getAccount(player.getName()) == null) {
            CubeConomy.logger().warning("Error creating / grabbing account for: " + player.getName());
        }
    }

    /**
     * Listens for plugin channel registration to send client updates
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onRegisterChannel(PlayerRegisterChannelEvent event) {
        if (event.getChannel().equals("cubeconomy")) CubeConomy.sendClientUpdate(event.getPlayer().getName());
    }

    /**
     * Listen for account balance changes to send updates to the client mod
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAccountReset(AccountResetEvent event) {
        CubeConomy.sendClientUpdate(event.getAccountName());
    }

    /**
     * Listen for account balance changes to send updates to the client mod
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAccountSet(AccountSetEvent event) {
        CubeConomy.sendClientUpdate(event.getAccountName());
    }

    /**
     * Listen for account balance changes to send updates to the client mod
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAccountUpdate(AccountUpdateEvent event) {
        CubeConomy.sendClientUpdate(event.getAccountName());
    }

    public class DeathListener implements Listener {

        private final boolean percentage = Constants.DeathFeePercentage;
        private final double amount = Constants.DeathFeeAmount;

        /**
         * Listen for player deaths to deduct a "death tax"
         *
         * @param event
         */
        @EventHandler(priority = EventPriority.MONITOR)
        public void onPlayerDeath(PlayerDeathEvent event) {
            Player player = event.getEntity();

            if (CubeConomy.worldguard != null) {
                RegionManager manager = CubeConomy.worldguard.getRegionManager(player.getWorld());
                ApplicableRegionSet set = manager.getApplicableRegions(player.getLocation());

                if (set.getFlag(DefaultFlag.RESPAWN_FEE) != null && !set.getFlag(DefaultFlag.RESPAWN_FEE)) {
                    // Player died in a no death tax region
                    return;
                }
            }

            Holdings holdings = CubeConomy.getAccount(player.getName()).getHoldings();
            double balance = holdings.balance();

            if (percentage)
                holdings.multiply((100 - amount) / 100);
            else
                holdings.subtract(balance > amount ? amount : balance);

            double diff = balance - holdings.balance();

            // Log in transaction database
            CubeConomy.getTransactions().insert(player.getName(), "[Death]", diff);

            Messaging.send(player, Template.parse("player.death", new String[]{"+amount,+money,+a,+m"}, new String[]{CubeConomy.format(diff)}));
        }
    }
}
