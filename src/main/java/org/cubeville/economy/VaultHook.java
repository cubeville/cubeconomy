package org.cubeville.economy;

import java.util.ArrayList;
import java.util.List;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

import org.cubeville.economy.system.Account;
import org.cubeville.economy.system.Holdings;
import org.cubeville.economy.util.Constants;

public class VaultHook implements Economy {

    private CubeConomy plugin;

    public VaultHook(CubeConomy plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean isEnabled() {
        return plugin.isEnabled();
    }

    @Override
    public String getName() {
        return plugin.getName();
    }

    @Override
    public String format(double amount) {
        return CubeConomy.format(amount);
    }

    @Override
    public String currencyNameSingular() {
        return Constants.Major.get(0);
    }

    @Override
    public String currencyNamePlural() {
        return Constants.Major.get(1);
    }

    @Override
    public boolean hasAccount(String name) {
        return CubeConomy.hasAccount(name);
    }

    @Override
    public double getBalance(String account) {
        return CubeConomy.getAccount(account).getHoldings().balance();
    }

    @Override
    public boolean has(String account, double amount) {
        return getBalance(account) >= amount;
    }

    @Override
    public EconomyResponse depositPlayer(String name, double amount) {
        Account account = CubeConomy.getAccount(name);
        Holdings holdings = account.getHoldings();
        holdings.add(amount);

        return new EconomyResponse(amount, holdings.balance(), ResponseType.SUCCESS, null);
    }

    @Override
    public EconomyResponse withdrawPlayer(String name, double amount) {
        Account account = CubeConomy.getAccount(name);
        Holdings holdings = account.getHoldings();

        if (!holdings.hasEnough(amount)) return new EconomyResponse(0, holdings.balance(), ResponseType.FAILURE, "Insufficient funds");

        holdings.subtract(amount);
        return new EconomyResponse(amount, holdings.balance(), ResponseType.SUCCESS, null);
    }

    @Override
    public boolean createPlayerAccount(String name) {
        if (hasAccount(name)) return false;

        CubeConomy.getAccount(name);
        return true;
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public List<String> getBanks() {
        return new ArrayList<String>();
    }

    @Override
    public EconomyResponse isBankMember(String arg0, String arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse isBankOwner(String arg0, String arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse createBank(String arg0, String arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse deleteBank(String arg0) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse bankBalance(String arg0) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse bankDeposit(String arg0, double arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse bankHas(String arg0, double arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

    @Override
    public EconomyResponse bankWithdraw(String arg0, double arg1) {
        return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, plugin.getName() + " does not support banks!");
    }

	@Override
	public int fractionalDigits() {
		return 2;
	}

}
