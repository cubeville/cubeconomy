package org.cubeville.economy.system;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import org.bukkit.Bukkit;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.events.AccountResetEvent;
import org.cubeville.economy.events.AccountSetEvent;
import org.cubeville.economy.events.AccountUpdateEvent;
import org.cubeville.economy.util.Constants;
import org.cubeville.economy.util.Misc;

/**
 * Controls player Holdings, and Bank Account holdings.
 *
 * @author Nijikokun
 */
public class Holdings {
    private String name = "";

    public Holdings(String name) {
        this.name = name;
    }

    public double balance() {
        return get();
    }

    private double get() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Double balance = Constants.Holdings;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();

            ps = conn.prepareStatement("SELECT * FROM " + Constants.SQLTable + " WHERE username = ? LIMIT 1");
            ps.setString(1, name);

            rs = ps.executeQuery();

            if (rs.next()) {
                balance = rs.getDouble("balance");
            }
        } catch (Exception e) {
            CubeConomy.logger().warning("Failed to grab holdings: " + e);
        } finally {
            CubeConomy.getiCoDatabase().close(ps, rs, conn);
        }

        return balance;
    }

    public void set(double balance) {
        AccountSetEvent Event = new AccountSetEvent(name, balance);
        Bukkit.getPluginManager().callEvent(Event);

        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();

            ps = conn.prepareStatement("UPDATE " + Constants.SQLTable + " SET `balance` = ? WHERE `username` = ?");
            ps.setDouble(1, balance);
            ps.setString(2, name);

            ps.executeUpdate();
        } catch (Exception e) {
            CubeConomy.logger().warning("Failed to set holdings: " + e);
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }
    }

    public void add(double amount) {
        double balance = get();
        double ending = (balance + amount);

        math(amount, balance, ending);
    }

    public void subtract(double amount) {
        double balance = get();
        double ending = (balance - amount);

        math(amount, balance, ending);
    }

    public void divide(double amount) {
        double balance = get();
        double ending = (balance / amount);

        math(amount, balance, ending);
    }

    public void multiply(double amount) {
        double balance = get();
        double ending = (balance * amount);

        math(amount, balance, ending);
    }

    public void reset() {
        AccountResetEvent Event = new AccountResetEvent(name);
        Bukkit.getPluginManager().callEvent(Event);

        if(!Event.isCancelled())
            set(Constants.Holdings);
    }

    private void math(double amount, double balance, double ending) {
        AccountUpdateEvent Event = new AccountUpdateEvent(name, balance, ending, amount);
        Bukkit.getPluginManager().callEvent(Event);

        if(!Event.isCancelled())
            set(ending);
    }

    public boolean isNegative() {
        return get() < 0.0;
    }

    public boolean hasEnough(double amount) {
        return amount <= get();
    }

    public boolean hasOver(double amount) {
        return amount < get();
    }

    public boolean hasUnder(double amount) {
        return amount > get();
    }

    @Override
    public String toString() {
        DecimalFormat formatter = new DecimalFormat("#,##0.00");
        Double balance = get();
        String formatted = formatter.format(balance);

        if (formatted.endsWith(".")) {
            formatted = formatted.substring(0, formatted.length() - 1);
        }

        return Misc.formatted(formatted, Constants.Major, Constants.Minor);
    }
}
