package org.cubeville.economy.system;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.util.Constants;
import org.cubeville.economy.util.Messaging;
import org.cubeville.economy.util.Template;

public class Interest extends TimerTask {
    Template Template = null;

    final double cutoff = Constants.InterestCutoff;
    final boolean percentage = Constants.InterestPercentage != 0.0;
    final double min = Constants.InterestMin;
    final double max = Constants.InterestMax;

    public Interest(String directory) {
        Template = new Template(directory, "messages.yml");
    }

    @Override
    public void run() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        double amount = 0.0;

        DecimalFormat DecimalFormat = new DecimalFormat("#.##");
        List<Account> accounts = new ArrayList<Account>();

        if (!percentage) {
            try {
                if (min > max) {
                    CubeConomy.logger().warning("Min interest cannot be greater than max interest");
                    return;
                } else if (min == max) {
                    amount = max;
                } else {
                    amount = Double.valueOf(DecimalFormat.format(Math.random() * (max - min) + (min)));
                }
            } catch (NumberFormatException e) {
                CubeConomy.logger().warning("Invalid Interest: " + e);
                return;
            }
        }

        if(Constants.InterestOnline) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                Account account = CubeConomy.getAccount(p.getName());

                if (isEligible(account)) accounts.add(account);
            }
        } else {
            conn = CubeConomy.getiCoDatabase().getConnection();

            try {
                String sql = "SELECT `username` FROM " + Constants.SQLTable;

                if (cutoff != 0.0) {
                    if (cutoff > 0.0)
                        sql += " WHERE `balance` < ?";
                    else
                        sql += " WHERE `balance` > ?";

                    ps = conn.prepareStatement(sql);
                    ps.setDouble(1, cutoff);
                } else {
                    ps = conn.prepareStatement(sql);
                }

                rs = ps.executeQuery();

                while(rs.next()) {
                    accounts.add(CubeConomy.getAccount(rs.getString("username")));
                }
            } catch(Exception E) {
                CubeConomy.logger().warning("Error executing query for interest: " + E.getMessage());
            } finally {
                CubeConomy.getiCoDatabase().close(ps, rs, conn);
            }
        }

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            conn.setAutoCommit(false);

            String updateSQL = "UPDATE " + Constants.SQLTable + " SET `balance` = ? WHERE `username` = ?";
            ps = conn.prepareStatement(updateSQL);

            for (Account account : accounts) {
                if (account != null) {
                    String name = account.getName();
                    Holdings holdings = account.getHoldings();

                    if(holdings != null) {
                        double balance = holdings.balance();

                        if(percentage) {
                            amount = Math.round((Constants.InterestPercentage * balance)/100);
                        }

                        ps.setDouble(1, balance + amount);
                        ps.setString(2, name);
                        ps.addBatch();

                        if(Constants.InterestAnn && Constants.InterestOnline) {
                            Messaging.send(
                                Bukkit.getPlayer(name),
                                Template.parse(
                                    "interest.announcement",
                                    new String[]{ "+amount,+money,+interest,+a,+m,+i" },
                                    new Object[]{ CubeConomy.format(amount) }
                                )
                            );
                        }

                        if(amount < 0.0)
                            CubeConomy.getTransactions().insert(name, "[Tax]", amount);
                        else
                            CubeConomy.getTransactions().insert("[Interest]", name, amount);

                    }
                }
            }

            //Execute the batch.
            ps.executeBatch();

            // Commit
            conn.commit();

            ps.clearBatch();
        } catch (BatchUpdateException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }
    }

    public boolean isEligible(Account account) {
        double balance = account.getHoldings().balance();

        if (cutoff > 0.0) {
            if (balance >= cutoff) return false;
        } else if (cutoff < 0.0) {
            if (balance <= cutoff) return false;
        }

        return true;
    }
}
