package org.cubeville.economy.system;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.util.Constants;

public class Transactions {

    /**
     * Inserts data into transaction without using seperate methods, direct method.
     *
     * @param from The player sending money
     * @param to The player receiving money
     * @param amount The amount of money transferred
     */
    public void insert(String from, String to, double amount) {
        if (!Constants.Logging)
            return;

        long timestamp = System.currentTimeMillis() / 1000;

        Object[] data = new Object[]{from, to, amount, timestamp};

        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("INSERT INTO " + Constants.SQLTable + "_Transactions(account_from, account_to, amount, `timestamp`) VALUES (?, ?, ?, ?)");

            int i = 1;
            for (Object obj : data) {
                ps.setObject(i++, obj);
            }

            ps.executeUpdate();
        } catch (SQLException e) {
        } finally {
            if(ps != null)
                try { ps.close(); } catch (SQLException ex) { }

            if(rs != null)
                try { rs.close(); } catch (SQLException ex) { }

            CubeConomy.getiCoDatabase().close(conn);
        }
    }
}