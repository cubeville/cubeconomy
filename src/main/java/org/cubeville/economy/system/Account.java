package org.cubeville.economy.system;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.events.AccountRemoveEvent;
import org.cubeville.economy.util.Constants;

public class Account {
    private String name;

    public Account(String name) {
        this.name = name;
    }

    /**
     * Gets the internal database ID of this account
     *
     * @return Database ID
     */
    public int getId() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        int id = -1;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `id` FROM " + Constants.SQLTable + " WHERE `username` = ? LIMIT 1");
            ps.setString(1, name);
            rs = ps.executeQuery();

            if(rs.next()) {
                id = rs.getInt("id");
            }
        } catch (Exception e) {
            id = -1;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, rs, conn);
        }

        return id;
    }

    /**
     * Gets the name of this account
     *
     * @return Account name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets this account's Holdings
     *
     * @return Holdings
     */
    public Holdings getHoldings() {
        return new Holdings(name);
    }

    /**
     * Checks if this account is hidden
     *
     * @return True if account is hidden
     */
    public boolean isHidden() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `hidden` FROM " + Constants.SQLTable + " WHERE username = ? LIMIT 1");
            ps.setString(1, name);
            rs = ps.executeQuery();

            if (rs != null) {
                if (rs.next()) {
                    return rs.getBoolean("hidden");
                }
            }
        } catch (Exception e) {
            CubeConomy.logger().warning("Failed to check status: " + e);
        } finally {
            CubeConomy.getiCoDatabase().close(ps, rs, conn);
        }

        return false;
    }

    /**
     * Sets this account's hidden state
     *
     * @param hidden True to hide account, false to unhide
     * @return True if account was updated successfully
     */
    public boolean setHidden(boolean hidden) {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();

            ps = conn.prepareStatement("UPDATE " + Constants.SQLTable + " SET `hidden` = ? WHERE `username` = ?");
            ps.setBoolean(1, hidden);
            ps.setString(2, name);

            ps.executeUpdate();
        } catch (Exception e) {
            CubeConomy.logger().warning("Failed to update status: " + e);
            return false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return true;
    }

    /**
     * Gets the ranking number of this account
     *
     * @return This account's rank
     */
    public int getRank() {
        int i = 0;
        int ret = -1;

        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `username` FROM " + Constants.SQLTable + " WHERE `hidden` = 0 ORDER BY `balance` DESC");
            rs = ps.executeQuery();

            while (rs.next()) {
                i++;

                if (rs.getString("username").equalsIgnoreCase(name)) {
                    ret = i;
                    break;
                }
            }
        } catch (Exception e) {
        } finally {
            CubeConomy.getiCoDatabase().close(ps, rs, conn);
        }

        return ret;
    }

    /**
     * Removes this account from the database
     */
    public void remove() {
        AccountRemoveEvent Event = new AccountRemoveEvent(name);
        Bukkit.getPluginManager().callEvent(Event);

        if(!Event.isCancelled()) {
            Connection conn = null;
            PreparedStatement ps = null;

            try {
                conn = CubeConomy.getiCoDatabase().getConnection();
                ps = conn.prepareStatement("DELETE FROM " + Constants.SQLTable + " WHERE `username` = ?");
                ps.setString(1, name);
                ps.executeUpdate();
            } catch(Exception e) {
                CubeConomy.logger().warning("Failed to remove account: " + e);
            } finally {
                CubeConomy.getiCoDatabase().close(ps, conn);
            }
        }
    }
}