package org.cubeville.economy.system;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.cubeville.economy.CubeConomy;
import org.cubeville.economy.util.Constants;

/**
 * Manage Account.
 *
 * @author Nijikokun
 */
public class Accounts {

    public Accounts() { }

    public boolean exists(String name) {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        boolean exists = false;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `username` FROM " + Constants.SQLTable + " WHERE `username` = ? LIMIT 1");
            ps.setString(1, name);
            rs = ps.executeQuery();
            exists = rs.next();
        } catch (Exception e) {
            exists = false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, rs, conn);
        }

        return exists;
    }

    public boolean create(String name) {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("INSERT INTO " + Constants.SQLTable + "(`username`, `balance`, `hidden`) VALUES (?, ?, 0)");
            ps.setString(1, name);
            ps.setDouble(2, Constants.Holdings);
            ps.executeUpdate();
        } catch (Exception e) {
            return false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return true;
    }

    public boolean remove(String name) {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("DELETE FROM " + Constants.SQLTable + " WHERE `username` = ? LIMIT 1");
            ps.setString(1, name);
            ps.executeUpdate();
        } catch (Exception e) {
            return false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return true;
    }

    public boolean purge() {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("DELETE FROM " + Constants.SQLTable + " WHERE `balance` = ?");
            ps.setDouble(1, Constants.Holdings);
            ps.executeUpdate();
        } catch (Exception e) {
            return false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return true;
    }

    /**
     * Removes all accounts from the database.
     * Do not use this.
     *
     * @return
     */
    public boolean emptyDatabase() {
        Connection conn = null;
        Statement ps = null;

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.createStatement();
            ps.execute("TRUNCATE TABLE " + Constants.SQLTable);
        } catch (Exception e) {
            return false;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return true;
    }

    public List<Double> values() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        List<Double> Values = new ArrayList<Double>();

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `balance` FROM " + Constants.SQLTable);
            rs = ps.executeQuery();

            while(rs.next()) {
                Values.add(rs.getDouble("balance"));
            }
        } catch (Exception e) {
            return null;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return Values;
    }

    public LinkedHashMap<String, Double> ranking(int amount) {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        LinkedHashMap<String, Double> Ranking = new LinkedHashMap<String, Double>();

        try {
            conn = CubeConomy.getiCoDatabase().getConnection();
            ps = conn.prepareStatement("SELECT `username`, `balance` FROM " + Constants.SQLTable + " WHERE `hidden` = 0 ORDER BY `balance` DESC LIMIT ?");
            ps.setInt(1, amount);
            rs = ps.executeQuery();

            while(rs.next()) {
                Ranking.put(rs.getString("username"), rs.getDouble("balance"));
            }
        } catch (Exception e) {
            CubeConomy.logger().warning(e.getMessage());
            return null;
        } finally {
            CubeConomy.getiCoDatabase().close(ps, conn);
        }

        return Ranking;
    }

    public Account get(String name) {
        if(exists(name)) {
            return new Account(name);
        } else {
            if(!create(name)) {
                return null;
            }
        }

        return new Account(name);
    }
}
