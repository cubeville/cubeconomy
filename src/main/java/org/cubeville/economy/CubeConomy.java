package org.cubeville.economy;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.cubeville.economy.entity.Players;
import org.cubeville.economy.net.Database;
import org.cubeville.economy.system.Account;
import org.cubeville.economy.system.Accounts;
import org.cubeville.economy.system.Interest;
import org.cubeville.economy.system.Transactions;
import org.cubeville.economy.util.Constants;
import org.cubeville.economy.util.Misc;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/**
 * iConomy by Team iCo
 *
 * @copyright     Copyright AniGaiku LLC (C) 2010-2011
 * @author          Nijikokun <nijikokun@gmail.com>
 * @author          Coelho <robertcoelho@live.com>
 * @author          ShadowDrakken <shadowdrakken@gmail.com>
 * @author          GoalieGuy6 <goalieguy6@cubeville.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CubeConomy extends JavaPlugin {
    public static Accounts Accounts = null;

    private static Server Server = null;
    private static Database Database = null;
    private static Transactions Transactions = null;
    private static Players playerListener = null;
    private static Timer Interest_Timer = null;

    private static Logger Logger = null;
    private static CubeConomy instance = null;

    public static WorldGuardPlugin worldguard = null;

    public static CubeConomy instance() {
        return instance;
    }

    @Override
    public void onEnable() {
        Locale.setDefault(Locale.US);

        // Get the server
        Server = getServer();
        Logger = getLogger();
        instance = this;

        getServer().getMessenger().registerOutgoingPluginChannel(this, "cubeconomy");

        // Lib Directory
        (new File("lib" + File.separator)).mkdir();
        (new File("lib" + File.separator)).setWritable(true);
        (new File("lib" + File.separator)).setExecutable(true);

        // Plugin Directory
        getDataFolder().mkdir();
        getDataFolder().setWritable(true);
        getDataFolder().setExecutable(true);

        // Setup the path.
        Constants.Plugin_Directory = getDataFolder().getPath();

        // Grab plugin details
        PluginDescriptionFile pdfFile = getDescription();

        // Default Files
        if (!new File(getDataFolder(), "config.yml").exists())
            saveResource("config.yml", false);
        if (!new File(getDataFolder(), "template.yml").exists())
            saveResource("template.yml", false);

        try {
            Constants.load(YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml")));
        } catch (Exception e) {
            Server.getPluginManager().disablePlugin(this);
            Logger.warning("Failed to retrieve configuration from directory.");
            Logger.warning("Please back up your current settings and let CubeConomy recreate it.");
            return;
        }

        try {
            Database = new Database();
            Database.setupAccountTable();
        } catch (Exception e) {
            Logger.severe("Database initialization failed: " + e);
            Server.getPluginManager().disablePlugin(this);
            return;

        }

        try {
            Transactions = new Transactions();
            Database.setupTransactionTable();
        } catch (Exception e) {
            Logger.warning("Could not load transaction logger: " + e);
        }

        // Initialize default systems
        Accounts = new Accounts();

        try {
            if (Constants.Interest) {
                long time = Constants.InterestSeconds * 1000L;

                Interest_Timer = new Timer();
                Interest_Timer.scheduleAtFixedRate(new Interest(getDataFolder().getPath()), time, time);
            }
        } catch (Exception e) {
            Logger.severe("Failed to start interest system: " + e);
            Server.getPluginManager().disablePlugin(this);
            return;
        }

        // Initializing Listeners
        playerListener = new Players(getDataFolder().getPath());

        // Event Registration
        playerListener.registerEvents(getServer().getPluginManager(), this);

        // Console Detail
        Logger.info("Developed by: " + pdfFile.getAuthors());

        hookVault();
        hookWorldGuard();
    }

    private void hookVault() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) return;

        try {
            Class.forName("net.milkbowl.vault.economy.Economy");
            VaultHook hook = new VaultHook(this);
            getServer().getServicesManager().register(Economy.class, hook, this, ServicePriority.Highest);
        } catch (Exception ex) {
            return;
        }
    }

    private void hookWorldGuard() {
    	Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

    	if (plugin == null || !(plugin instanceof WorldGuardPlugin)) return;

    	worldguard = (WorldGuardPlugin) plugin;
    }

    @Override
    public void onDisable() {
        if (Interest_Timer != null) {
            Interest_Timer.cancel();
        }

        Server = null;
        Logger = null;
        Accounts = null;
        Database = null;
        Transactions = null;
        playerListener = null;
        Interest_Timer = null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        String[] split = new String[args.length + 1];
        split[0] = cmd.getName().toLowerCase();
        System.arraycopy(args, 0, split, 1, args.length);

        playerListener.onPlayerCommand(sender, split);
        return true;
    }

    /**
     * Formats the holding balance in a human readable form with the currency attached:<br /><br />
     * 20000.53 = 20,000.53 Coin<br />
     * 20000.00 = 20,000 Coin
     *
     * @param account The name of the account you wish to be formatted
     * @return Formatted account balance
     */
    public static String format(String account) {
        return getAccount(account).getHoldings().toString();
    }

    /**
     * Formats the money in a human readable form with the currency attached:<br /><br />
     * 20000.53 = 20,000.53 Coin<br />
     * 20000.00 = 20,000 Coin
     *
     * @param amount The amount of money to format
     * @return Formatted string
     */
    public static String format(double amount) {
        DecimalFormat formatter = new DecimalFormat("#,##0.00");
        String formatted = formatter.format(amount);

        if (formatted.endsWith(".")) {
            formatted = formatted.substring(0, formatted.length() - 1);
        }

        return Misc.formatted(formatted, Constants.Major, Constants.Minor);
    }

    /**
     * Grab an account, if it doesn't exist, create it.
     *
     * @param name The name of the account to get
     * @return Account or null
     */
    public static Account getAccount(String name) {
        return Accounts.get(name);
    }

    /**
     * Checks if an account exists
     *
     * @param name The name of the account to check
     * @return True if account exists
     */
    public static boolean hasAccount(String name) {
        return Accounts.exists(name);
    }

    /**
     * Grabs Database controller.
     *
     * @return Database
     */
    public static Database getiCoDatabase() {
        return Database;
    }

    /**
     * Grabs Transaction Log Controller.
     *
     * Used to log transactions between a player and anything. Such as the
     * system or another player or just environment.
     *
     * @return Transaction manager
     */
    public static Transactions getTransactions() {
        return Transactions;
    }

    /**
     * Check and see if the sender has the permission as designated by node.
     *
     * @param sender The sender to check permissions on
     * @param node The permission node to check for
     * @return boolean True if sender has node
     */
    public static boolean hasPermissions(CommandSender sender, String node) {
        return (sender instanceof Player) ? sender.hasPermission(node) : true;
    }

    /**
     * Gets the plugin logger
     *
     * @return Logger
     */
    public static Logger logger() {
        return Logger;
    }

    /**
     * Sends a balance update to the specified player if they are online
     *
     * @param name The player to send an update to
     */
    public static void sendClientUpdate(final String name) {
        Bukkit.getScheduler().runTaskLater(instance, new Runnable() {
            @Override
            public void run() {
                Player player = Bukkit.getPlayerExact(name);
                if (player != null) player.sendPluginMessage(instance, "cubeconomy", format(player.getName()).getBytes());
            }
        }, 1L);
    }

}
